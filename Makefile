.PHONY: format
format:
	isort .
	autoflake --remove-all-unused-imports --remove-unused-variables --in-place .
	black --config pyproject.toml .

.PHONY: lint
lint:
	isort --check-only .
	black --config pyproject.toml --check .
	mypy --config pyproject.toml .
	flake8 --config=.flake8
	pylint --rcfile=pyproject.toml --output-format=text tcp_twisted
	pydocstyle --config pyproject.toml .
	bandit -c pyproject.toml -r .

.PHONY: test
test:
	mkdir -p reports > /dev/null 2>&1 || true
	pytest \
			--cov=tcp_twisted \
			--cov-report=term-missing:skip-covered \
			--cov-report html:reports/html \
			--cov-report xml:reports/coverage.xml  \
			--junitxml=reports/xunit.xml \
			--exitfirst \
			--capture=sys \
			tests

.PHONY: clean
clean:
	rm -rf `find . -name __pycache__`
	rm -f `find . -type f -name '*.py[co]' `
	rm -f `find . -type f -name '*~' `
	rm -f `find . -type f -name '.*~' `
	rm -rf .cache
	rm -rf .mypy_cache
	rm -rf .pytest_cache
	rm -rf *.egg-info
	rm -f .coverage
	rm -f **/.coverage.*
	rm -rf reports
	rm -rf build
	rm -rf dist

.PHONY: init
init:
	pip install -r requirements.txt -r requirements-dev.txt
	make clean && make format && make lint

.PHONE: package
package: clean
	pip install --upgrade pip setuptools wheel build
	pip install -r requirements.txt
	python3 -m build --wheel
