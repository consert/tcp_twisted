# TCP Twisted

## Description

A simple restartable TCP server written in Python using the [Twisted](https://twisted.org/) engine.

## Requirements

* Python 3.8+
* Twisted (tested with 23.8.0)

## Usage

```shell
pip install --upgrade pip  # use latest pip version
pip install git+https://gitlab.com/consert/tcp_twisted.git
```

```python
import logging
from typing import Callable

from tcp_twisted import Server


LOG = logging.getLogger(__name__)


def on_message(message, reply_handler):
    # type: (str, Callable[[str], None]) -> None
    """On message callback.

    Parameters
    ----------
    message : str
        Message received.
    reply_handler : Callable[[str], None]
        Reply handler: send a reply to the client.
    """
    LOG.info("received: %s", message)
    reply_handler("ACK")


server = Server(port=1234, on_message=on_message)
# or to just start listening:
# server = Server(port=1234, on_message=lambda m, r: None)
server.start()
# ...
server.stop()
# or server.restart()
# later on ...
server.start()
# ...
# finally
server.stop()
```
