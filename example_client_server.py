"""Example client for the server."""

import logging
import socket
import sys
import threading
import time
from contextlib import closing
from pathlib import Path
from typing import Any, Callable

try:
    from tcp_twisted import Server
except ImportError:
    sys.path.append(str(Path(__file__).parent))
    from tcp_twisted import Server


logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s %(levelname)s %(message)s",
    handlers=[logging.StreamHandler(stream=sys.stdout)],
)
LOGGER = logging.getLogger("tcp::example::client")


def get_tcp_client(host: str, port: int) -> socket.socket:
    """Connect to the server."""
    addr = (host, port)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sock.connect(addr)
    except ConnectionRefusedError:
        print("Connection failed. Is the server running?")
        sys.exit(1)
    return sock


def get_available_port():
    # type: () -> int
    """Get an available port.

    Returns
    -------
    int
        Available port.
    """
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as soc:
        soc.bind(('', 0))
        soc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return soc.getsockname()[1]


def on_server_message(message, reply_handler):
    # type: (str, Callable[[str], Any]) -> None
    """On message callback.

    Parameters
    ----------
    message : str
        Message received on the server.
    reply_handler : Callable[[str], Any]
        Reply handler: send a reply to the client.
    """
    msg = message.replace('\r\n', '')
    LOGGER.info("Server received: %s", msg)
    LOGGER.info("Server sending after 2 seconds a reply")
    time.sleep(2)
    reply_handler(msg + " ACK")


def on_client_message(message):
    # type: (str) -> None
    """On message callback.

    Parameters
    ----------
    message : str
        Message received on the client.
    """
    LOGGER.info("Client received: %s", message)


def start_client_listener(client, on_message):
    # type: (socket.socket, Callable[[str], None]) -> None
    """Start the client listener.

    Parameters
    ----------
    client : socket.socket
        Client socket.
    on_message : Callable[[str], None]
        On message callback.
    """
    while True:
        data = client.recv(1024)
        if not data:
            break
        on_message(data.decode('ascii'))


def server_client(port):
    # type: (int) -> None
    """Start and stop the server and client normally ."""
    server = Server(port=port, on_message=on_server_message)
    server.start()
    client = get_tcp_client('localhost', server.port)
    threading.Thread(target=start_client_listener, args=(client, on_client_message)).start()
    client.sendall(b"hello world 1")
    time.sleep(5)
    LOGGER.info("stopping server")
    client.close()
    server.stop()


def server_client_client(port):
    # type: (int) -> None
    """Start the server and the client. Then restart the client."""
    server = Server(port=port, on_message=on_server_message)
    server.start()
    client = get_tcp_client('localhost', port)
    threading.Thread(target=start_client_listener, args=(client, on_client_message)).start()
    client.sendall(b"hello world 2")
    time.sleep(5)
    LOGGER.info("stopping client")
    client.close()
    client = get_tcp_client('localhost', port)
    threading.Thread(target=start_client_listener, args=(client, on_client_message)).start()
    client.sendall(b"hello world 3")
    time.sleep(5)
    LOGGER.info("stopping client")
    client.close()
    LOGGER.info("stopping server")
    server.stop()


def server_client_server(port):
    # type: (int) -> None
    """Start the server and the client. Then restart the server."""
    server = Server(port=port, on_message=on_server_message)
    server.start()
    client = get_tcp_client('localhost', port)
    threading.Thread(target=start_client_listener, args=(client, on_client_message)).start()
    client.sendall(b"hello world 4")
    time.sleep(5)
    LOGGER.info("stopping server")
    server.stop()
    server = Server(port=port, on_message=on_server_message)
    server.start()
    # now we need to get a new client
    client = get_tcp_client('localhost', port)
    threading.Thread(target=start_client_listener, args=(client, on_client_message)).start()
    client.sendall(b"hello world 5")
    time.sleep(5)
    LOGGER.info("stopping client")
    client.close()
    LOGGER.info("stopping server")
    server.stop()


def main():
    # type: () -> None
    """Start the main program."""
    port = get_available_port()
    server_client(port)
    server_client_client(port)
    server_client_server(port)


if __name__ == "__main__":
    main()
