"""Example Server."""
import logging
import socket
import sys
import time
from contextlib import closing
from pathlib import Path
from typing import Callable

try:
    from tcp_twisted import Server
except ImportError:
    sys.path.append(str(Path(__file__).parent))
    from tcp_twisted import Server

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s %(levelname)s %(message)s",
    handlers=[logging.StreamHandler(stream=sys.stdout)],
)
LOGGER = logging.getLogger("tcp::example::client")


def get_available_port():
    # type: () -> int
    """Get an available port.

    Returns
    -------
    int
        Available port.
    """
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as soc:
        soc.bind(('', 0))
        soc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return soc.getsockname()[1]


def on_message(message, reply_handler):
    # type: (str, Callable[[str], None]) -> None
    """On message callback.

    Parameters
    ----------
    message : str
        Message received.
    reply_handler : Callable[[str], None]
        Reply handler: send a reply to the client.
    """
    LOGGER.info("received: %s", message)
    reply_handler("ACK")


def main():
    # type: () -> None
    """Start the main program."""
    port = get_available_port()
    duration = 5
    with Server(port=port, on_message=on_message) as server:
        LOGGER.info("server started for %s seconds", duration)
        time.sleep(duration)
        LOGGER.info("stopping server")
    # alternative:
    server = Server(port=port, on_message=on_message)
    LOGGER.info("starting server for %s seconds", duration)
    server.start()
    time.sleep(duration)
    server.stop()
    LOGGER.info("server stopped")
    server = Server(port=port, on_message=on_message)
    LOGGER.info("starting server for %s seconds", duration)
    server.start()
    time.sleep(duration)
    LOGGER.info("restarting server for %s seconds", duration)
    server.restart()
    time.sleep(duration)
    LOGGER.info("stopping server")
    server.stop()


if __name__ == "__main__":
    main()
