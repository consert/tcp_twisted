"""TCP Server using Twisted."""
import sys
from pathlib import Path

try:
    from .server import Server
except ImportError:  # pragma: no cover
    sys.path.append(str(Path(__file__).parent.parent))
    from tcp_twisted.server import Server

__version__ = "0.1.0"
__all__ = ["Server", "__version__"]
