"""Factory for the client."""
import sys
from pathlib import Path
from typing import Any, Callable

from twisted.internet.interfaces import IAddress
from twisted.internet.protocol import Factory as FactoryBase
from twisted.internet.protocol import Protocol as ProtocolBase

try:
    from .protocol import Protocol
except ImportError:  # pragma: no cover
    sys.path.append(str(Path(__file__).parent.parent))
    from tcp_twisted.protocol import Protocol


class Factory(FactoryBase):
    """Messenger factory."""

    def buildProtocol(self, addr):
        # type: (IAddress) -> ProtocolBase
        """Build a protocol.

        Parameters
        ----------
        addr : IAddress
            Address of the client.
        """
        return Protocol(self)

    def __init__(self, on_message):
        # type: (Callable[[str,  Callable[[str], Any]], None]) -> None
        """Create a new messenger factory.

        Parameters
        ----------
        on_message : Callable[[str, Callable[[str], Any]], None]
            Callback for received message:
            `on_message(message, reply_handler)`
            reply_handler is a callback to send a reply to the client.
        """
        self.callback = on_message

    def on_message(self, data, reply_handler):
        # type: (str, Callable[[str], Any]) -> None
        """On Data received.

        Parameters
        ----------
        data : str
            Data received.
        reply_handler : Callable[[str], Any]
            Reply handler: send a reply to the client.
        """
        self.callback(data, reply_handler)
