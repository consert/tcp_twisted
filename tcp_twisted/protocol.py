"""Messenger protocol."""

from functools import partial

from twisted.internet.interfaces import ITransport
from twisted.internet.protocol import Factory as FactoryBase
from twisted.internet.protocol import Protocol as ProtocolBase


class Protocol(ProtocolBase):
    """Messenger protocol."""

    def __init__(self, factory):
        # type: (FactoryBase) -> None
        """Create a new messenger protocol."""
        self.factory = factory

    @staticmethod
    def send_reply(message, transport):
        # type: (str, ITransport) -> None
        """Send a message."""
        transport.write(f"{message}\r\n".encode(encoding='ascii'))

    def dataReceived(self, data):
        # type: (bytes) -> None
        """On Data received."""
        if self.factory is not None and hasattr(self.factory, "on_message"):
            try:
                string = data.decode(encoding='ascii')
            except UnicodeDecodeError:  # pragma: no cover
                pass
            else:
                reply_handler = partial(self.send_reply, transport=self.transport)
                self.factory.on_message(string, reply_handler)
