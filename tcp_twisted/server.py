"""Server."""
import logging
import sys
from pathlib import Path
from typing import Any, Callable, Optional

try:
    from .wrapper import ServerWrapper
except ImportError:  # pragma: no cover
    sys.path.append(str(Path(__file__).parent.parent))
    from tcp_twisted.wrapper import ServerWrapper


LOGGER = logging.getLogger("tcp::server")


class Server:
    """Simple reusable TCP server."""

    _wrapper: Optional[ServerWrapper] = None

    def __init__(self, port, on_message, timeout=None, interface=''):
        # type: (int, Callable[[str, Callable[[str], Any]], None],Optional[float], str) -> None
        """Create a new server.

        Parameters
        ----------
        port : int
            Port to listen on.
        on_message : Callable[[str, Callable[[str], Any]], None]
            Callback for received message:
            `on_message(message, reply_handler)`
            reply_handler is a callback to send a reply to the client.
        timeout : Optional[float]
            Timeout for the server.
        interface : str
            Interface to listen on. Defaults to '' (all interfaces)
        """
        self._port = port
        self._timeout = timeout
        self._on_message = on_message
        self._interface = interface
        self._init_wrapper()
        self._running = False

    @property
    def running(self):
        # type: () -> bool
        """Get running state."""
        return self._running

    @property
    def port(self):
        # type: () -> int
        """Get port."""
        return self._port

    @property
    def timeout(self):
        # type: () -> Optional[float]
        """Get timeout."""
        return self._timeout

    @property
    def on_message(self):
        # type: () -> Callable[[str, Callable[[str], Any]], None]
        """Get on_message."""
        return self._on_message

    @property
    def interface(self):
        # type: () -> str
        """Get interface."""
        return self._interface

    def _init_wrapper(self):
        # type: () -> None
        """Initialize the wrapper."""
        self._wrapper = ServerWrapper(
            port=self.port,
            on_message=self.on_message,
            timeout=self.timeout,
            interface=self.interface,
        )

    def start(self):
        # type: () -> None
        """Start the server."""
        if self.running:  # pragma: no cover
            return
        if not self._wrapper:  # pragma: no cover
            raise RuntimeError("ServerWrapper not initialized")
        self._wrapper.start()
        self._running = True

    def stop(self):
        # type: () -> None
        """Stop the server."""
        if not self._running:  # pragma: no cover
            return
        if not self._wrapper:  # pragma: no cover
            raise RuntimeError("ServerWrapper not initialized")
        self._wrapper.stop()
        self._running = False
        del self._wrapper
        self._wrapper = None

    def __enter__(self):
        # type: () -> Server
        """Enter the context manager."""
        self.start()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        # type: (Any, Any, Any) -> None
        """Exit the context manager."""
        self.stop()

    def restart(self):
        # type: () -> None
        """Restart the server."""
        self.stop()
        self._init_wrapper()
        self.start()


__all__ = ["Server"]
