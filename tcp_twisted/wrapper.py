"""Wrapper for TCP server."""
# pylint: disable=import-outside-toplevel,no-member,reimported,unused-import,redefined-outer-name
import logging
import sys
from pathlib import Path
from threading import Thread
from typing import Any, Callable, Optional, cast

from twisted.internet import reactor
from twisted.internet.interfaces import IReactorCore
from twisted.internet.tcp import Port

try:
    from tcp_twisted.factory import Factory
except ImportError:  # pragma: no cover
    sys.path.append(str(Path(__file__).parent.parent))
    from tcp_twisted.factory import Factory

LOGGER = logging.getLogger("tcp::server")


# hack to allow 'restarting' the reactor
if "twisted.internet.reactor" in sys.modules:
    del sys.modules['twisted.internet.reactor']


def get_reactor():
    # type: () -> IReactorCore
    """Get the reactor."""
    if "twisted.internet.reactor" in sys.modules:
        del sys.modules['twisted.internet.reactor']
    import twisted.internet.error
    from twisted.internet import reactor  # noqa
    from twisted.internet import default

    try:
        default.install()
    except twisted.internet.error.ReactorAlreadyInstalledError:  # pragma: no cover
        pass
    # cast it so mypy doesn't complain a lot
    reactor_cast = cast(IReactorCore, reactor)
    return reactor_cast


class TCPServer(Thread):
    """Threaded TCP server."""

    reactor = None  # type: Optional[IReactorCore]  # noqa
    factory = None  # type: Optional[Factory]  # noqa

    def __init__(self, interface, port, on_message, timeout=None):
        # type: (str, int, Callable[[str,  Callable[[str], Any]], None], Optional[float]) -> None
        """Create a new TCP server.

        Parameters
        ----------
        interface : str
            Interface to listen on. Defaults to '' (all interfaces)
        port : int
            Port to listen on.
        on_message : Callable[[str, Callable[[str], Any]], None]
            Callback for received message:
            `on_message(message, reply_handler)`
            reply_handler is a callback to send a reply to the client.
        timeout : Optional[float]
            Timeout for the server.
        """
        super().__init__()
        from twisted.internet.endpoints import TCP4ServerEndpoint

        self.timeout = timeout
        self.reactor = get_reactor()
        endpoint = TCP4ServerEndpoint(self.reactor, port, interface=interface)
        deferred = endpoint.listen(Factory(on_message=on_message))
        deferred.addCallback(callback=self.on_start)

    def on_start(self, port):
        # type: (Port) -> None
        """On connect callback."""
        socket = port.getHost()
        LOGGER.info(
            "listening on %s:%s",
            socket.host,
            socket.port,
        )
        self.factory = port.factory

    def run(self):
        # type: () -> None
        """Start the server."""
        if self.reactor is None:  # pragma: no cover
            raise RuntimeError("reactor is not running")
        if not self.reactor.running:  # type: ignore
            self.reactor.run(installSignalHandlers=False)  # type: ignore


class ServerWrapper:
    """Server Wrapper."""

    server: TCPServer
    timeout: float

    def __init__(self, interface, port, on_message, timeout=None):
        # type: (str, int, Callable[[str,  Callable[[str], Any]], None],Optional[float]) -> None
        """Create a new TCP server.

        Parameters
        ----------
        interface : str
            Interface to listen on. Defaults to '' (all interfaces)
        port : int
            Port to listen on.
        """
        self.timeout = timeout if timeout is not None else 0.2
        self.server = TCPServer(
            interface=interface, port=port, on_message=on_message, timeout=self.timeout
        )

    def start(self):
        # type: () -> None
        """Start the server."""
        if self.server is None:  # pragma: no cover
            raise RuntimeError("server is not running")
        self.server.start()

    def stop(self):
        # type: () -> None
        """Stop the server."""
        self.server.reactor.callFromThread(self.server.reactor.stop)  # type: ignore
        self.server.join()
