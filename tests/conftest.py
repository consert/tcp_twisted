"""Common fixtures for tests."""

import socket
from contextlib import closing
from typing import Callable

import pytest

# pylint: disable=unused-import
try:
    from tcp_twisted import Server
except ImportError:
    import sys
    from pathlib import Path

    sys.path.append(str(Path(__file__).parent.parent))
    from tcp_twisted import Server


@pytest.fixture(scope="session", autouse=True)  # type: ignore
def port():
    # type: () -> int
    """Get an available port."""
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as soc:
        soc.bind(('', 0))
        soc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return soc.getsockname()[1]


def get_tcp_client(_port):
    # type: (int) -> socket.socket
    """Connect to the server."""
    addr = ('0.0.0.0', _port)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sock.connect(addr)
    except ConnectionRefusedError:
        print("Connection failed. Is the server running?")
        sys.exit(1)
    return sock


@pytest.fixture(scope="session")  # type: ignore
def client():
    # type: () -> Callable[[Server], socket.socket]
    """Get a client."""

    def wrapped(server):
        # type: (Server) -> socket.socket
        """Get a client."""
        return get_tcp_client(server.port)

    return wrapped
