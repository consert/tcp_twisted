"""Test tcp_twisted.Server."""

import socket
from typing import Callable

from tcp_twisted import Server


def test_start_server(port):
    # type: (int) -> None
    """Test starting the server."""
    server = Server(port=port, on_message=lambda message, reply_handler: None)
    server.start()
    server.stop()


def test_send_message(client, port):
    # type: (Callable[[Server], socket.socket], int) -> None
    """Test sending a message."""
    server = Server(port=port, on_message=lambda message, reply_handler: None)
    server.start()
    sock = client(server)
    sock.sendall(b'hello')
    sock.close()
    server.stop()


def test_on_message(client, port):
    # type: (Callable[[Server], socket.socket], int) -> None
    """Test on_message."""
    message = 'hello'

    def on_message(data, reply_handler):
        # type: (str, Callable[[str], None]) -> None
        """On message callback."""
        assert data == message
        reply_handler(data)

    server = Server(port=port, on_message=on_message)
    server.start()
    sock = client(server)
    sock.sendall(message.encode('utf-8'))
    reply = sock.recv(1024).decode('utf-8')
    assert reply.replace('\r\n', '') == message.replace('\r\n', '')
    sock.close()
    server.stop()


def test_server_context_manager(port):
    # type: (int) -> None
    """Test server context manager."""
    with Server(port=port, on_message=lambda message, reply_handler: None) as server:
        assert server.running is True
    assert server.running is False


def test_restart_server(port):
    # type: (int) -> None
    """Test restarting the server."""
    server = Server(port=port, on_message=lambda message, reply_handler: None)
    server.start()
    assert server.running is True
    server.stop()
    assert server.running is False
    server.restart()
    assert server.running is True
    server.stop()
    assert server.running is False
